# 텍스트 데이터 처리와 분석 <sup>[1](#footnote_1)</sup>

> <font size="3">다양한 기법을 활용하여 텍스트 데이터를 처리하고 분석하는 방법을 알아본다.</font>

## 목차

1. [개요](./text-data-processing-and-analysis.md#intro)
1. [텍스트 데이터 전처리](./text-data-processing-and-analysis.md#sec_02)
1. [텍스트 데이터 표현](./text-data-processing-and-analysis.md#sec_03)
1. [텍스트 데이터 분류](./text-data-processing-and-analysis.md#sec_04)
1. [텍스트 데이터 클러스터링](./text-data-processing-and-analysis.md#sec_05)
1. [텍스트 데이터 요약](./text-data-processing-and-analysis.md#sec_06)
1. [텍스트 데이터 생성](./text-data-processing-and-analysis.md#sec_07)
1. [텍스트 데이터 시각화](./text-data-processing-and-analysis.md#sec_08)
1. [마치며](./text-data-processing-and-analysis.md#summary)

<a name="footnote_1">1</a>: [ML Tutorial 24 — Text Data Processing and Analysis](https://medium.datadriveninvestor.com/ml-tutorial-24-text-data-processing-and-analysis-57ba2b284688?sk=63be675039409f90f1e49077bdb64397)를 편역하였습니다.
